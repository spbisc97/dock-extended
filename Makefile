.PHONY:
.DEFAULT_GOAL := help


define PRINT_HELP_PYSCRIPT
import re, sys
print("\x1b[1m%-20s\x1b[0m%s" % ("usage:", "make [COMMAND]"))
print("\x1b[1m%-20s %s\x1b[0m" % ("COMMAND", "Description"))
for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("\x1b[92m%-20s \x1b[0m%s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT
export UUID=dash-extended@spb97r.me

help:
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

debug_log: ## Debug log
	@journalctl -f -o cat GNOME_SHELL_EXTENSION_UUID=UUID

test_nested: ## Test extension in nested Gnome Shell
	@dbus-run-session -- gnome-shell --nested --wayland

# lg - Extension lg manager, run ALT+F2
