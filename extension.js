/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const { Gio, Shell, Meta,St } = imports.gi;
const PanelMenu = imports.ui.panelMenu
const Main = imports.ui.main
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Dash = imports.ui.dash
const ExtensionUtils = imports.misc.extensionUtils;
const DashExtended_KeyboardShortcuts_NUM_HOTKEYS = 10;


let settings = ExtensionUtils.getSettings("org.gnome.shell.extensions.dash-extended");



class Extension {
    constructor() {
    }

    enable() {
        log(`enabling ${Me.metadata.name}`);

        let indicatorName = `${Me.metadata.name} Indicator`;
        
        // Create a panel button
        this._indicator = new PanelMenu.Button(0.0, indicatorName, false);
        
        // Add an icon
        let icon = new St.Icon({
            gicon: new Gio.ThemedIcon({name: 'face-laugh-symbolic'}),
            style_class: 'system-status-icon'
        });
        this._indicator.add_child(icon);

        // `Main.panel` is the actual panel you see at the top of the screen,
        // not a class constructor.
        Main.panel.addToStatusArea(indicatorName, this._indicator);

        let apps = AppFavorites.getAppFavorites().getFavorites();
        // let app = apps[target - 1];
        // if (app) {
            // Main.overview.hide();
            // app.activate();
        // }


        let keys = ['app-ctrl-hotkey-'];
        keys.forEach(function (key) {
            for (let i = 0; i < DashExtended_KeyboardShortcuts_NUM_HOTKEYS; i++) {
                let appNum = i;
                Main.wm.addKeybinding(
                    key + (i + 1), 
                settings,
                Meta.KeyBindingFlags.IGNORE_AUTOREPEAT,
                Shell.ActionMode.NORMAL | Shell.ActionMode.OVERVIEW,
                () => {
                    apps[i].activate(1);
                    });
            }
        }, this);
    }

    disable() {
        log(`disabling ${Me.metadata.name}`);

        this._indicator.destroy();
        this._indicator = null;    }
}

function init() {
    log(`initializing ${Me.metadata.name}`);
    return new Extension();
}

function activate(button) {
    let event = Clutter.get_current_event();
    let modifiers = event ? event.get_state() : 0;
    let isMiddleButton = button && button == Clutter.BUTTON_MIDDLE;
    let isCtrlPressed = (modifiers & Clutter.ModifierType.CONTROL_MASK) != 0;
    let openNewWindow = this.app.can_open_new_window() &&
                        this.app.state == Shell.AppState.RUNNING &&
                        (isCtrlPressed || isMiddleButton);

    if (this.app.state == Shell.AppState.STOPPED || openNewWindow)
        this.animateLaunch();

    if (openNewWindow)
        this.app.open_new_window(-1);
    else
        this.app.activate();

    Main.overview.hide();
}

